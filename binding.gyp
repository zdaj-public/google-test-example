{
  "targets": [ 
    {
      "target_name": "test",
      'type': 'executable',
      "sources": [
        "./lib/gtest/gtest.h",
        "./lib/gtest-all.cc",
        "./test/main_test.cpp",
        "./test/test_test.cpp"
        ],
      'msvs_settings': {
        'VCCLCompilerTool': {
          'WarningLevel': 3,
          'ExceptionHandling': 1,
          'AdditionalOptions': [ "/std:c++17", "/MT", "/Gy", "/O2" ]
        }
      },
      "include_dirs": [
        "lib",
        "lib/gtest"
      ]
    },
  ]
}
